import React from 'react'
import clsx from 'clsx'

import styles from './field-group.module.scss'

interface Props {
  className?: string
  error?: string
}

const FieldGroup: React.FC<Props> = ({className, error, children}) => {
  return (
    <div className={clsx('relative pb-4', className)}>
      {children}
      {Boolean(error) && <div className={clsx('p-2 text-xs', styles.error)}>{error}</div>}
    </div>
  )
}

export default FieldGroup
