import React from 'react'
import clsx from 'clsx'

import styles from './text-input.module.scss'

interface Props {
  value: string
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  onBlur: (e: React.FocusEvent<HTMLInputElement>) => void
  className?: string
  placeholder?: string
  error?: boolean
  name: string
}

const TextInput = ({className, error, ...forwardProps}: Props) => {
  return <input className={clsx('rounded p-2', error && styles.error, styles.root, className)} {...forwardProps} />
}

export default TextInput
