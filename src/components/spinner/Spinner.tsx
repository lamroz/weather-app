import React from 'react'
import Loader from 'react-loader-spinner'

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css'

const Spinner = () => {
  return (
    <div className="fixed top-0 left-0 w-full h-full flex items-center justify-center bg-gray-300 bg-opacity-40 z-10">
      <Loader type="Oval" color="#3e4958" height={50} width={50} />
    </div>
  )
}

export default Spinner
