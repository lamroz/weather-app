import React from 'react'
import clsx from 'clsx'

import styles from './generic-error-page.module.scss'

interface Props {
  status: number | string
  message: string
}

const GenericErrorPage = ({status, message}: Props) => {
  return (
    <div className="w-full flex flex-col items-center  mt-16">
      <div className={clsx('font-bold', styles.status_code)}>{status}</div>

      <div className="max-w-2xl mt-4 text-sm text-center" dangerouslySetInnerHTML={{__html: message}} />
    </div>
  )
}

export default GenericErrorPage
