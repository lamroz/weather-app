import React from 'react'
import clsx from 'clsx'

import styles from './button.module.scss'

interface Props {
  label: string
  onClick?: () => void
  className?: string
  type?: 'button' | 'submit' | 'reset'
}

const Button = ({label, className, ...forwardProps}: Props) => {
  return (
    <button
      className={clsx('py-2 px-4 text-sm font-semibold shadow-md rounded', styles.button, className)}
      {...forwardProps}
    >
      {label}
    </button>
  )
}

export default Button
