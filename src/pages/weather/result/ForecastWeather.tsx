import React from 'react'
import {useTranslation} from 'react-i18next'
import {useParams} from 'react-router-dom'
import {useTypedSelector} from 'store'
import {selectedCityDailyWeatherSelector} from '../store/selectors'
import ForecastWeatherItem from './ForecastWeatherItem'

const ForecastWeather = () => {
  const {t} = useTranslation()

  const {city} = useParams<{city: string}>()

  const selectedCityForecast = useTypedSelector(selectedCityDailyWeatherSelector)

  return (
    <div className="w-full mt-4 mb-8">
      <div className="p-4 text-sm">{t('message.forecastWeatherForCity', {city})}</div>

      <ul className="lg:flex">
        {selectedCityForecast.map((item, index) => {
          const {icon, description} = item.weather[0]

          return (
            <ForecastWeatherItem
              key={index}
              date={item.dt}
              temp={item.temp.day}
              description={description}
              icon={icon}
            />
          )
        })}
      </ul>
    </div>
  )
}

export default ForecastWeather
