import React from 'react'
import {WeatherPropertyTypes} from 'api/weather/model'
import i18n from 'i18n'

interface Props {
  value: number
  valueToCompare: number
  label: string
  type: WeatherPropertyTypes
}

// bigerLowerTranslations[0] = bigger value translation
// bigerLowerTranslations[1] = lower value translation
const parseResultMessage = (
  value: number,
  baseTranslation: string,
  bigerLowerTranslations: (string | string)[],
  sameValueTranslation = 'sameValue'
): string => {
  const count = Math.abs(value)

  if (value > 0) {
    return i18n.t(`message.${baseTranslation}`, {count, type: i18n.t(`message.${bigerLowerTranslations[0]}`)})
  }

  if (value < 0) {
    return i18n.t(`message.${baseTranslation}`, {count, type: i18n.t(`message.${bigerLowerTranslations[1]}`)})
  }

  return i18n.t(`message.${sameValueTranslation}`)
}

const getResultMessage = (value: number, valueToCompare: number, type: WeatherPropertyTypes): string | null => {
  const finalValue = value - valueToCompare

  switch (type) {
    case WeatherPropertyTypes.TEMP: {
      return parseResultMessage(finalValue, 'temperatureLevel', ['hotter', 'colder'])
    }
    case WeatherPropertyTypes.PRESSURE: {
      return parseResultMessage(finalValue, 'pressureLevel', ['higher', 'lower'])
    }
    case WeatherPropertyTypes.HUMIDITY: {
      return parseResultMessage(finalValue, 'humidityLevel', ['higherFemine', 'lowerFemine'])
    }
    default:
      return null
  }
}

const WeatherPropertyComparison = ({value, valueToCompare, type, label}: Props) => {
  return (
    <div className="flex flex-col mb-4">
      <span className="text-xs text-gray-400">{label}</span>
      <span className="text-xs mt-2 font-semibold">{getResultMessage(value, valueToCompare, type)}</span>
    </div>
  )
}

export default WeatherPropertyComparison
