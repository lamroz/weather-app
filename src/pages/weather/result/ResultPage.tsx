import React, {Fragment, useCallback, useEffect, useState} from 'react'
import {useHistory, useParams} from 'react-router-dom'
import {useDispatch} from 'react-redux'

import CitySearchForm from '../common/CitySearchForm'
import SelectedCityData from './SelectedCityData'
import ResultsComparison from './ResultsComparison'
import {getSelectedCityForecast, getOtherCitiesWeather} from '../store/actions'
import ForecastWeather from './ForecastWeather'
import {ThunkDispatchResult} from 'store/actions'
import {hideSpinner, showSpinner} from 'store/actions/spinner'

const ResultPage = () => {
  const history = useHistory()
  const {city} = useParams<{city: string}>()
  const dispatch = useDispatch<ThunkDispatchResult>()

  const [loading, setLoading] = useState<boolean>(false)

  const handleFetchEnd = useCallback(() => {
    setLoading(false)
    dispatch(hideSpinner())
  }, [])

  useEffect(() => {
    if (city) {
      setLoading(true)
      dispatch(showSpinner())

      dispatch(getSelectedCityForecast(city))
        .then(() => {
          dispatch(getOtherCitiesWeather(city, ['Warszawa', 'Gdańsk', 'Kraków', 'Wrocław']))
          handleFetchEnd()
        })
        .catch(() => {
          handleFetchEnd()

          history.push('/error/not-found', {city})
        })
    }
  }, [city])

  return (
    <div className="w-full flex flex-col items-center">
      <CitySearchForm initialCity={city} />

      {!loading && (
        <Fragment>
          <div className="w-full flex flex-col lg:flex-row mt-2">
            <SelectedCityData className="lg:mr-2 md:mb-0 mb-2" />
            <ResultsComparison className="lg:ml-2 md:mt-0 mt-2" />
          </div>
          <ForecastWeather />
        </Fragment>
      )}
    </div>
  )
}

export default ResultPage
