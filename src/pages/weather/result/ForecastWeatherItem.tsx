import React from 'react'

import {formatValueToLoaclDate} from 'utils/date.utils'
import CelciusValue from '../common/CelciusValue'
import WeatherTypeIcon from '../common/WeatherTypeIcon'

interface Props {
  date: number
  icon: string
  description: string
  temp: number
}

const ForecastWeatherItem = ({date, icon, description, temp}: Props) => {
  return (
    <li className="w-full flex flex-col items-center overflow-hidden bg-white rounded-lg shadow-md lg:mr-2 lg:ml-2 mb-4 p-2">
      <span className="text-xs font-bold">{formatValueToLoaclDate(date * 1000)}</span>
      <WeatherTypeIcon type={icon} className="w-24 h-24" />

      <div className="bg-gray-100 p-4 rounded-lg border w-full flex flex-col justify-between text-center items-center h-32">
        <span className="text-xs break">{description}</span>

        <CelciusValue
          fontSize="lg:text-4xl text-6xl"
          degreeFontSize="lg:text-xl text-2xl"
          className="mt-2"
          value={Math.round(temp)}
        />
      </div>
    </li>
  )
}

export default ForecastWeatherItem
