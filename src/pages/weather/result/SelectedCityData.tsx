import React, {useMemo} from 'react'
import clsx from 'clsx'
import {useSelector} from 'react-redux'
import {useTranslation} from 'react-i18next'
import {useParams} from 'react-router-dom'

import {RootState} from 'store/reducers'
import {WindIcon, HumidityIcon, PressureIcon} from 'assets/icons'

import WeatherPropertyItem from './WeatherPropertyItem'
import CelciusValue from '../common/CelciusValue'
import {formatTimestampToLoaclDateTime} from 'utils/date.utils'
import WeatherTypeIcon from '../common/WeatherTypeIcon'
import {useTypedSelector} from 'store'
import {selectedCityCurrentWeatherSelector} from '../store/selectors'
import {formatValueWithUnit, UnitTypes} from 'utils/text.utils'

interface Props {
  className?: string
}

const SelectedCityData = ({className}: Props) => {
  const {t} = useTranslation()
  const {city} = useParams<{city: string}>()

  const selectedCityWeather = useTypedSelector(selectedCityCurrentWeatherSelector)

  const weatherUpdateDateTime = useMemo((): string => {
    if (selectedCityWeather) {
      return formatTimestampToLoaclDateTime(selectedCityWeather.dt * 1000)
    }
    return ''
  }, [selectedCityWeather])

  if (!selectedCityWeather) {
    return null
  }

  // API returns weather data as an array of objects, so we need to extract first item, which contains weather details
  const weather = selectedCityWeather.weather[0]

  return (
    <div className={clsx('w-full p-4 flex flex-col overflow-hidden bg-white rounded-lg shadow-md', className)}>
      <div>
        <div className="text-4xl font-bold text">{city}</div>
        <div className="text-xs text-gray-400 mt-2">
          {t('label.updatedDateTime', {dateTime: weatherUpdateDateTime})}
        </div>
      </div>

      <div className="mt-8 flex lg:flex-row flex-col justify-center items-center">
        <WeatherTypeIcon type={weather.icon} />

        <div className="flex flex-col justify-center lg:p-8 p-2 text-center">
          <CelciusValue className="font-bold mb-2" value={Math.round(selectedCityWeather.temp)} />
          <div>{weather.description}</div>
        </div>
      </div>

      <div className="flex w-full bg-gray-100 p-4 rounded-lg border">
        <WeatherPropertyItem
          value={formatValueWithUnit(selectedCityWeather.wind_speed, UnitTypes.KILOMETERS_PER_HOUR)}
          label={t('label.windSpeed')}
          showSeparator
        >
          <WindIcon width={32} />
        </WeatherPropertyItem>
        <WeatherPropertyItem
          value={formatValueWithUnit(selectedCityWeather.pressure, UnitTypes.HECTOPASCALS)}
          label={t('label.pressure')}
          showSeparator
        >
          <PressureIcon width={32} />
        </WeatherPropertyItem>
        <WeatherPropertyItem
          value={formatValueWithUnit(selectedCityWeather.humidity, UnitTypes.PERCENTAGES)}
          label={t('label.humidity')}
        >
          <HumidityIcon width={32} />
        </WeatherPropertyItem>
      </div>
    </div>
  )
}

export default SelectedCityData
