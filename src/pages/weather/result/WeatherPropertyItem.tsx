import React from 'react'
import clsx from 'clsx'

import styles from './weather-property-item.module.scss'

interface Props {
  value: string | number
  label: string
  showSeparator?: boolean
}

const WeatherPropertyItem: React.FC<Props> = ({value, label, showSeparator, children}) => {
  return (
    <div className={clsx('flex w-full flex-col items-center', showSeparator && styles.separator)}>
      <div className="h-10">{children}</div>
      <span className="text-xs text-gray-400 mt-2 mb-4 lg:h-auto h-8 text-center">{label}</span>
      <span className="text-xl font-bold">{value}</span>
    </div>
  )
}

export default WeatherPropertyItem
