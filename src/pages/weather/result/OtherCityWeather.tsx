import React from 'react'
import clsx from 'clsx'
import {useTranslation} from 'react-i18next'

import {WeatherPropertyTypes} from 'api/weather/model'

import WeatherTypeIcon from '../common/WeatherTypeIcon'
import WeatherPropertyComparison from './WeatherPropertyComparison'
import styles from './other-city-weather.module.scss'

interface Props {
  showSeparator?: boolean
  icon: string
  description: string
  cityName: string
  cityWeather: {
    temp: number
    pressure: number
    humidity: number
  }
  selectedCityWeather: {
    temp: number
    pressure: number
    humidity: number
  }
}

const OtherCityWeather = ({showSeparator, icon, cityName, description, cityWeather, selectedCityWeather}: Props) => {
  const {t} = useTranslation()

  const {temp, pressure, humidity} = cityWeather
  const {temp: selectedCityTemp, pressure: selectedCityPressure, humidity: selectedCityHumidity} = selectedCityWeather

  return (
    <li
      className={clsx(
        'flex flex-col items-center text-center w-full lg:mb-0 mb-4 p-2 overflow-hidden',
        showSeparator && styles.separator
      )}
    >
      <span className="text-sm font-bold">{cityName}</span>

      <div className="h-32 flex flex-col items-center">
        <WeatherTypeIcon type={icon} size="@2x" />
        <span className="text-xs break overflow-hidden">{description}</span>
      </div>

      <div className="mt-8">
        <WeatherPropertyComparison
          value={temp}
          valueToCompare={selectedCityTemp}
          label={t('label.temperature')}
          type={WeatherPropertyTypes.TEMP}
        />
        <WeatherPropertyComparison
          value={pressure}
          valueToCompare={selectedCityPressure}
          label={t('label.pressure')}
          type={WeatherPropertyTypes.PRESSURE}
        />
        <WeatherPropertyComparison
          value={humidity}
          valueToCompare={selectedCityHumidity}
          label={t('label.humidity')}
          type={WeatherPropertyTypes.HUMIDITY}
        />
      </div>
    </li>
  )
}

export default OtherCityWeather
