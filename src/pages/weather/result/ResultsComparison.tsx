import React, {useMemo} from 'react'
import clsx from 'clsx'

import {useTypedSelector} from 'store'

import OtherCityWeather from './OtherCityWeather'
import {selectedCityCurrentWeatherSelector} from '../store/selectors'

interface Props {
  className?: string
}

const ResultsComparison = ({className}: Props) => {
  const {otherCitiesWeather} = useTypedSelector((store) => store.weather)

  const selectedCityWeather = useTypedSelector(selectedCityCurrentWeatherSelector)

  const parsedSelectedCityWeather = useMemo(() => {
    const {temp, humidity, pressure} = selectedCityWeather ?? {}
    return {
      temp: Math.round(temp),
      humidity: Math.round(humidity),
      pressure: Math.round(pressure)
    }
  }, [selectedCityWeather])

  return (
    <ul
      className={clsx('w-full p-4 flex lg:flex-row flex-col overflow-hidden bg-white rounded-lg shadow-md', className)}
    >
      {otherCitiesWeather.map((city, index) => {
        const weather = city.weather[0]
        const {temp, humidity, pressure} = city.main

        return (
          <OtherCityWeather
            key={city.id}
            showSeparator={index + 1 < otherCitiesWeather.length}
            icon={weather.icon}
            description={weather.description}
            cityName={city.name}
            cityWeather={{
              temp: Math.round(temp),
              humidity: Math.round(humidity),
              pressure: Math.round(pressure)
            }}
            selectedCityWeather={parsedSelectedCityWeather}
          />
        )
      })}
    </ul>
  )
}

export default ResultsComparison
