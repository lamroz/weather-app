import React, {Fragment} from 'react'
import {useLocation} from 'react-router-dom'
import {useTranslation} from 'react-i18next'

import {GenericErrorPage} from 'components'

import CitySearchForm from '../common/CitySearchForm'

const CityNotFound = () => {
  const {t} = useTranslation()

  const location = useLocation<{city: string}>()

  return (
    <Fragment>
      <CitySearchForm />

      <GenericErrorPage status={404} message={t('message.cityNotFound', {city: location?.state?.city})} />
    </Fragment>
  )
}

export default CityNotFound
