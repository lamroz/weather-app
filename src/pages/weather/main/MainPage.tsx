import React from 'react'

import CitySearchForm from '../common/CitySearchForm'

const MainPage = () => {
  return (
    <div className="w-full flex justify-center mt-16">
      <CitySearchForm />
    </div>
  )
}

export default MainPage
