import React from 'react'

interface Props {
  type: string
  size?: string
  className?: string
}

const WeatherTypeIcon = ({type, size = '@4x', className}: Props) => {
  return (
    <img className={className} src={`http://openweathermap.org/img/wn/${type}${size}.png`} alt="weather type icon" />
  )
}

export default WeatherTypeIcon
