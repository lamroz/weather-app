import React from 'react'
import clsx from 'clsx'

interface Props {
  value: string | number
  className?: string
  degreeFontSize?: string
  fontSize?: string
}

const CelciusValue = ({value, className, fontSize = 'text-6xl', degreeFontSize = 'text-2xl'}: Props) => {
  return (
    <div className={clsx('flex items-center justify-center', fontSize, className)}>
      {value}
      <div className={clsx(fontSize, 'inline-flex items-start font-normal ml-2 text-gray-200')}>
        <span className={clsx('text-gray-200', degreeFontSize)}>o</span>C
      </div>
    </div>
  )
}

export default CelciusValue
