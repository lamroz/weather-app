import React, {useCallback} from 'react'
import {useTranslation} from 'react-i18next'
import {useHistory} from 'react-router-dom'
import {Formik} from 'formik'

import {Button, TextInput, FieldGroup} from 'components'

interface Props {
  initialCity?: string
}

const CitySearchForm = ({initialCity = ''}: Props) => {
  const {t} = useTranslation()
  const history = useHistory()

  const onSubmit = (values) => {
    history.push(`/${values.city}`)
  }

  const validate = useCallback((values) => {
    const errors: {[key: string]: string} = {}

    if (!values.city) {
      errors.city = t('validation.required')
    }

    return errors
  }, [])

  return (
    <div className="w-full">
      <Formik initialValues={{city: initialCity}} validate={validate} onSubmit={onSubmit}>
        {({handleSubmit, handleChange, handleBlur, values, errors}) => {
          const hasError = Boolean(errors.city)

          return (
            <form onSubmit={handleSubmit} className="w-full flex items-start">
              <FieldGroup className="w-full mr-2" error={hasError && (errors.city as string)}>
                <TextInput
                  name="city"
                  className="w-full"
                  placeholder={t('action.typeCityName')}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  value={values.city}
                  error={hasError}
                />
              </FieldGroup>

              <Button label={t('action.search')} type="submit" className="ml-2" onClick={handleSubmit} />
            </form>
          )
        }}
      </Formik>
    </div>
  )
}

export default CitySearchForm
