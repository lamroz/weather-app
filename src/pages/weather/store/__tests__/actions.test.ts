import {setSelectedCityForecast, setOtherCitiesWeather} from '../actions'
import {SET_SELECTED_CITY_FORECAST, SET_OTHER_CITIES_WEATHER} from '../types'

import {selectedCityForecast, weatherData} from '__data__/mock-data'

describe('Testing weather action creators', () => {
  it('should create an action to set selected city forecast', () => {
    const expectedAction = {
      type: SET_SELECTED_CITY_FORECAST,
      payload: selectedCityForecast
    }

    expect(setSelectedCityForecast(selectedCityForecast)).toEqual(expectedAction)
  })

  it('should create an action to set other cities weather', () => {
    const expectedAction = {
      type: SET_OTHER_CITIES_WEATHER,
      payload: weatherData
    }

    expect(setOtherCitiesWeather(weatherData)).toEqual(expectedAction)
  })
})
