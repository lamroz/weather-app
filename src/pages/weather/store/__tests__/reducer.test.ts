import weatherReducer from '../reducer'

import {selectedCityForecast, weatherData} from '__data__/mock-data'
import {SET_OTHER_CITIES_WEATHER, SET_SELECTED_CITY_FORECAST} from '../types'

describe('Testing weather reducer', () => {
  it('should return an empty state', () => {
    const expectedState = {
      selectedCityForecast: null,
      otherCitiesWeather: []
    }
    expect(
      weatherReducer({selectedCityForecast: null, otherCitiesWeather: []}, {type: null, payload: undefined})
    ).toEqual(expectedState)
  })

  it('should return selected city forecast', () => {
    const expectedState = {
      selectedCityForecast: selectedCityForecast,
      otherCitiesWeather: []
    }
    expect(
      weatherReducer(
        {selectedCityForecast: null, otherCitiesWeather: []},
        {type: SET_SELECTED_CITY_FORECAST, payload: selectedCityForecast}
      )
    ).toEqual(expectedState)
  })

  it('should return other cities weather', () => {
    const expectedState = {
      selectedCityForecast: null,
      otherCitiesWeather: weatherData
    }
    expect(
      weatherReducer(
        {selectedCityForecast: null, otherCitiesWeather: []},
        {type: SET_OTHER_CITIES_WEATHER, payload: weatherData}
      )
    ).toEqual(expectedState)
  })
})
