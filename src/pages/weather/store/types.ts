import {CityForecastWeather, CityWeatherDetails} from 'api/weather/model'

export const SET_SELECTED_CITY_FORECAST = '[WEATHER] SET_SELECTED_CITY_FORECAST'
export const SET_OTHER_CITIES_WEATHER = '[WEATHER] SET_OTHER_CITIES_WEATHER'

interface SetSelectedCityForecastAction {
  type: typeof SET_SELECTED_CITY_FORECAST
  payload: CityForecastWeather
}

interface SetOtherCitiesWeatherAction {
  type: typeof SET_OTHER_CITIES_WEATHER
  payload: CityWeatherDetails[]
}

export type WeatherActionTypes = SetSelectedCityForecastAction | SetOtherCitiesWeatherAction
