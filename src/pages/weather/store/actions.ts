import {AxiosError, AxiosResponse} from 'axios'

import API from 'api'
import {CityForecastWeather, CityWeatherDetails} from 'api/weather/model'

import {WeatherActionTypes, SET_OTHER_CITIES_WEATHER, SET_SELECTED_CITY_FORECAST} from './types'
import {ThunkDispatchResult, ThunkResult} from 'store/actions'

export const setSelectedCityForecast = (weatherData: CityForecastWeather): WeatherActionTypes => ({
  type: SET_SELECTED_CITY_FORECAST,
  payload: weatherData
})

export const setOtherCitiesWeather = (weatherData: CityWeatherDetails[]): WeatherActionTypes => ({
  type: SET_OTHER_CITIES_WEATHER,
  payload: weatherData
})

export const getSelectedCityForecast = (city: string): ThunkResult<Promise<CityForecastWeather>> => (
  dispatch: ThunkDispatchResult
) => {
  return new Promise((resolve, reject) => {
    API.weather
      .getWeatherForecastByCity(city)
      .then((response) => {
        dispatch(setSelectedCityForecast(response.data))

        return resolve(response.data)
      })
      .catch((error: AxiosError) => {
        dispatch(setSelectedCityForecast(null))

        console.error(error)
        return reject(error)
      })
  })
}

export const getOtherCitiesWeather = (city: string, otherCities: string[] = []): ThunkResult<void> => {
  return (dispatch) => {
    const otherCitiesPromiseChain: Promise<AxiosResponse<CityWeatherDetails>>[] = otherCities.reduce((acc, item) => {
      if (item !== city) {
        acc.push(API.weather.getWeatherDetailsByCity(item))
      }
      return acc
    }, [])

    Promise.all(otherCitiesPromiseChain)
      .then((response) => {
        const otherCitiesParsedWeather = response.map((item) => item.data)

        dispatch(setOtherCitiesWeather(otherCitiesParsedWeather))
      })
      .catch(() => Promise.reject())
  }
}
