import {CityForecastWeather, CityWeatherDetails} from 'api/weather/model'

import {WeatherActionTypes, SET_OTHER_CITIES_WEATHER, SET_SELECTED_CITY_FORECAST} from './types'

export interface WeatherState {
  selectedCityForecast: CityForecastWeather
  otherCitiesWeather: CityWeatherDetails[]
}

const initialState: WeatherState = {
  selectedCityForecast: null,
  otherCitiesWeather: []
}

const weatherReducer = (state = initialState, action: WeatherActionTypes): WeatherState => {
  switch (action.type) {
    case SET_SELECTED_CITY_FORECAST: {
      return {
        ...state,
        selectedCityForecast: action.payload
      }
    }
    case SET_OTHER_CITIES_WEATHER: {
      return {
        ...state,
        otherCitiesWeather: action.payload
      }
    }
    default:
      return state
  }
}

export default weatherReducer
