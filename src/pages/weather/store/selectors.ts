import {createSelector} from 'reselect'

import {RootState} from 'store/reducers'

const selectedCityForecastSelector = (state: RootState) => state.weather.selectedCityForecast

export const selectedCityCurrentWeatherSelector = createSelector(
  selectedCityForecastSelector,
  (selectedCityForecast) => {
    return selectedCityForecast?.current ?? null
  }
)

export const selectedCityDailyWeatherSelector = createSelector(selectedCityForecastSelector, (selectedCityForecast) => {
  // First item in the daily forecast is related to the current day, so we can skip it
  return selectedCityForecast?.daily.slice(1) ?? []
})
