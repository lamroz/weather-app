import React from 'react'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import Layout from 'layout/Layout'

const MainPage = React.lazy(() => import('./weather/main/MainPage'))
const ResultPage = React.lazy(() => import('./weather/result/ResultPage'))
const CityNotFound = React.lazy(() => import('./weather/not-found/CityNotFound'))

const Routes = () => {
  return (
    <Router>
      <Layout>
        <Switch>
          <Route path="/" exact>
            <MainPage />
          </Route>
          <Route path="/error/not-found">
            <CityNotFound />
          </Route>
          <Route path="/:city">
            <ResultPage />
          </Route>
        </Switch>
      </Layout>
    </Router>
  )
}

export default Routes
