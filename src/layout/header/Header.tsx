import React from 'react'
import {Link} from 'react-router-dom'
import clsx from 'clsx'

import styles from './header.module.scss'

const Header = () => {
  return (
    <div className={clsx('w-full flex items-center justify-center shadow-lg relative', styles.header)}>
      <div className="text-xl font-black max-w-screen-xl w-full lg:text-left lg:px-16 sm:px-4 text-center">
        <Link to="/">
          <i className="fas fa-cloud-sun mr-2" /> <span className={styles.logo_text}>Weather</span>App
        </Link>
      </div>
    </div>
  )
}

export default Header
