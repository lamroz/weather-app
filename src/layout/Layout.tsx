import React from 'react'
import {Spinner} from 'components'

import Header from './header/Header'
import {useTypedSelector} from 'store'

const Layout: React.FC = ({children}) => {
  const showSpinner = useTypedSelector((state) => state.spinner.showSpinner)

  return (
    <div className="w-full">
      {Boolean(showSpinner) && <Spinner />}
      <Header />

      <div className="max-w-screen-xl w-full lg:mt-16 mt-8 lg:px-16 px-4 mx-auto">{children}</div>
    </div>
  )
}

export default Layout
