export const selectedCityForecast = {
  lat: 52.2298,
  lon: 21.0118,
  timezone: 'Europe/Warsaw',
  timezone_offset: 3600,
  current: {
    dt: 1615930281,
    sunrise: 1615870073,
    sunset: 1615912881,
    temp: 3.32,
    feels_like: -0.98,
    pressure: 1013,
    humidity: 87,
    dew_point: 1.37,
    uvi: 0,
    clouds: 0,
    visibility: 10000,
    wind_speed: 3.6,
    wind_deg: 320,
    weather: [{id: 800, main: 'Clear', description: 'bezchmurnie', icon: '01n'}]
  },
  daily: [
    {
      dt: 1615888800,
      sunrise: 1615870073,
      sunset: 1615912881,
      temp: {day: 6.73, min: 1.34, max: 8.52, night: 3.09, eve: 7.18, morn: 1.58},
      feels_like: {day: 3.13, night: -1.05, eve: 2.88, morn: -1.24},
      pressure: 1012,
      humidity: 58,
      dew_point: -0.78,
      wind_speed: 2.12,
      wind_deg: 354,
      weather: [{id: 500, main: 'Rain', description: 'słabe opady deszczu', icon: '10d'}],
      clouds: 26,
      pop: 0.73,
      rain: 0.44,
      uvi: 1.62
    }
  ]
}

export const weatherData = [
  {
    coord: {lon: 17.0333, lat: 51.1},
    weather: [{id: 803, main: 'Clouds', description: 'pochmurno z przejaśnieniami', icon: '04n'}],
    base: 'stations',
    main: {temp: 2.86, feels_like: -2.23, temp_min: 2.22, temp_max: 3.89, pressure: 1017, humidity: 87},
    visibility: 10000,
    wind: {speed: 4.63, deg: 310},
    clouds: {all: 75},
    dt: 1615929880,
    sys: {type: 1, id: 1715, country: 'PL', sunrise: 1615871014, sunset: 1615913850},
    timezone: 3600,
    id: 3081368,
    name: 'Wrocław',
    cod: 200
  },
  {
    coord: {lon: 19.9167, lat: 50.0833},
    weather: [{id: 803, main: 'Clouds', description: 'pochmurno z przejaśnieniami', icon: '04n'}],
    base: 'stations',
    main: {temp: 3.35, feels_like: -1.82, temp_min: 2.78, temp_max: 3.89, pressure: 1013, humidity: 81},
    visibility: 10000,
    wind: {speed: 4.63, deg: 310},
    clouds: {all: 75},
    dt: 1615929941,
    sys: {type: 1, id: 1701, country: 'PL', sunrise: 1615870312, sunset: 1615913168},
    timezone: 3600,
    id: 3094802,
    name: 'Kraków',
    cod: 200
  }
]
