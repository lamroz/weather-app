export {default as WindIcon} from './WindIcon'
export {default as HumidityIcon} from './HumidityIcon'
export {default as PressureIcon} from './PressureIcon'

export interface IconProps {
  width?: number
  height?: number
}
