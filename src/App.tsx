import React, {Suspense} from 'react'
import {Provider} from 'react-redux'

import Routes from 'pages/Routes'
import store from 'store'
import {Spinner} from 'components'

import 'assets/styles/global.scss'

const App = () => {
  return (
    <Suspense fallback={<Spinner />}>
      <Provider store={store}>
        <Routes />
      </Provider>
    </Suspense>
  )
}

export default App
