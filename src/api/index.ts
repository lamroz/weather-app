import axios from 'axios'

import WeatherAPIFactory from './weather/index'

const axiosInstance = axios.create({
  baseURL: 'https://api.openweathermap.org/data/2.5'
})

axiosInstance.interceptors.request.use((config) => {
  config.params = config.params || {}
  config.params['APPID'] = 'd537321cc468ca33d868e6f2db7895cc'
  config.params['lang'] = 'pl'
  config.params['units'] = 'metric'

  return config
})

const API = {
  weather: WeatherAPIFactory(axiosInstance)
}

export default API
