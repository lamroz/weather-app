import API from '../index'
import mockAxios from 'jest-mock-axios'

import {selectedCityForecast, weatherData} from '__data__/mock-data'

const getResponseObject = (data: any) => ({
  data,
  status: 200,
  statusText: 'OK',
  headers: {},
  config: {}
})

describe('Testing weather API', () => {
  afterEach(() => {
    mockAxios.reset()
  })

  it('should fetch weather by city name', () => {
    const catchFn = jest.fn()
    const thenFn = jest.fn()

    const city = 'Warszawa'

    API.weather.getWeatherDetailsByCity(city).then(thenFn).catch(catchFn)

    expect(mockAxios.get).toHaveBeenCalledWith('/weather', {params: {q: city}})
    expect(mockAxios.get).toHaveBeenCalledTimes(1)

    const response = getResponseObject(weatherData)

    mockAxios.mockResponse({data: weatherData})

    expect(thenFn).toHaveBeenCalledWith(response)

    expect(catchFn).not.toHaveBeenCalled()
  })

  it('should fetch forecast by city coords', () => {
    const catchFn = jest.fn()
    const thenFn = jest.fn()

    const coords = {lat: 54.3521, lon: 18.6464}

    API.weather.getWeatherForecastByCoords(coords).then(thenFn).catch(catchFn)

    expect(mockAxios.get).toHaveBeenCalledWith('/onecall', {params: {...coords, exclude: 'minutely,hourly,alerts'}})
    expect(mockAxios.get).toHaveBeenCalledTimes(1)

    const response = getResponseObject(selectedCityForecast)

    mockAxios.mockResponse({data: selectedCityForecast})

    expect(thenFn).toHaveBeenCalledWith(response)

    expect(catchFn).not.toHaveBeenCalled()
  })

  it('should fetch forecast by city name', () => {
    const catchFn = jest.fn()
    const thenFn = jest.fn()

    const city = 'Warszawa'

    API.weather.getWeatherForecastByCity(city).then(thenFn).catch(catchFn)

    expect(mockAxios.get).toHaveBeenCalledWith('/weather', {params: {q: city}})
    expect(mockAxios.get).toHaveBeenCalledTimes(1)

    const fetchWeatherRequest = mockAxios.getReqByUrl('/weather')

    mockAxios.mockResponse({data: weatherData[0]}, fetchWeatherRequest)

    const fetchForecastRequest = mockAxios.lastReqGet()

    mockAxios.mockResponse({data: selectedCityForecast}, fetchForecastRequest)

    const response = getResponseObject(selectedCityForecast)

    expect(mockAxios.get).toHaveBeenCalledTimes(2)

    expect(thenFn).toHaveBeenCalledWith(response)
    expect(catchFn).toHaveBeenCalledTimes(0)
  })
})
