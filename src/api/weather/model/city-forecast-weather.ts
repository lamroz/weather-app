import {Weather} from './weather'

interface CurrentWeather {
  dt: number
  sunrise: number
  sunset: number
  temp: number
  feels_like: number
  pressure: number
  humidity: number
  dew_point: number
  uvi: number
  clouds: number
  visibility: number
  wind_speed: number
  wind_deg: number
  weather: Weather[]
  rain?: {
    '1h': number
  }
}

interface DailyWeather {
  dt: number
  sunrise: number
  sunset: number
  temp: {
    day: number
    min: number
    max: number
    night: number
    eve: number
    morn: number
  }
  feels_like: {
    day: number
    night: number
    eve: number
    morn: number
  }
  pressure: number
  humidity: number
  dew_point: number
  wind_speed: number
  wind_deg: number
  weather: Weather[]
  clouds: number
  pop: number
  rain: number
  uvi: number
}

export interface CityForecastWeather {
  lat: number
  lon: number
  timezone: string
  timezone_offset: number
  current: CurrentWeather
  daily: DailyWeather[]
}
