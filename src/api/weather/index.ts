import {AxiosInstance} from 'axios'

import {CityForecastWeather, CityWeatherDetails} from './model'

export default (axios: AxiosInstance) => {
  const getWeatherDetailsByCity = (city: string) => {
    return axios.get<CityWeatherDetails>('/weather', {params: {q: city}})
  }

  const getWeatherForecastByCoords = (coord: {lat: number; lon: number}) => {
    return axios.get<CityForecastWeather>('/onecall', {
      // We want only current and daily weather, so other types are excluded
      params: {lat: coord.lat, lon: coord.lon, exclude: 'minutely,hourly,alerts'}
    })
  }

  const getWeatherForecastByCity = (city: string) => {
    /* 
      In order to get a forecast weather for a specific city from OpenWeather API we need to have coordinates of that city.
      We can extract this type of information from another endpoint (`/weather`) - by passing a name of the city.
      This is a temporary solution - dedicated API should support getting forecast weather by a city name.
    */
    return getWeatherDetailsByCity(city).then(({data: {coord}}) => getWeatherForecastByCoords(coord))
  }

  return {
    getWeatherDetailsByCity,
    getWeatherForecastByCoords,
    getWeatherForecastByCity
  }
}
