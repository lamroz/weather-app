import {formatValueWithUnit, UnitTypes} from '../text.utils'

describe('Testing text utils', () => {
  it('should return value with percentages', () => {
    expect(formatValueWithUnit(81, UnitTypes.PERCENTAGES)).toBe('81 %')
  })
})
