import {formatValueToLoaclDate, formatTimestampToLoaclDateTime} from '../date.utils'

describe('Testing date utils', () => {
  it('should format timestamp value to date format', () => {
    expect(formatValueToLoaclDate(1615967900364)).toBe('17.03.2021')
  })

  it('should format date object to date format', () => {
    expect(formatValueToLoaclDate(new Date(1615967900364))).toBe('17.03.2021')
  })

  it('should format timestamp value to date time format', () => {
    expect(formatTimestampToLoaclDateTime(1615967900364)).toBe('17.03.2021 08:58:20')
  })
})
