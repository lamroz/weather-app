export enum UnitTypes {
  PERCENTAGES = 'PERCENTAGES',
  HECTOPASCALS = 'HECTOPASCALS',
  KILOMETERS_PER_HOUR = 'KILOMETERS_PER_HOUR'
}

export const formatValueWithUnit = (value: string | number, unit: UnitTypes): string | number => {
  switch (unit) {
    case UnitTypes.HECTOPASCALS:
      return `${value} hPa`
    case UnitTypes.PERCENTAGES:
      return `${value} %`
    case UnitTypes.KILOMETERS_PER_HOUR:
      return `${value} km/h`
    default:
      return value
  }
}
