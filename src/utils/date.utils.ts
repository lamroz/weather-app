export const formatValueToLoaclDate = (value: number | Date) => {
  if (value instanceof Date) {
    return value.toLocaleDateString()
  }

  const dateTime = new Date(value)

  return dateTime.toLocaleDateString()
}

export const formatTimestampToLoaclDateTime = (value: number) => {
  const dateTime = new Date(value)

  return `${formatValueToLoaclDate(dateTime)} ${dateTime.toLocaleTimeString()}`
}
