export const SHOW_SPINNER = '[SPINNER] SHOW_SPINNER'
export const HIDE_SPINNER = '[SPINNER] HIDE_SPINNER'

interface ShowSpinnerAction {
  type: typeof SHOW_SPINNER
}

interface HideSpinnerAction {
  type: typeof HIDE_SPINNER
}

export type SpinnerActionTypes = ShowSpinnerAction | HideSpinnerAction

export const showSpinner = (): ShowSpinnerAction => ({
  type: SHOW_SPINNER
})

export const hideSpinner = (): HideSpinnerAction => ({
  type: HIDE_SPINNER
})
