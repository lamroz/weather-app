import {showSpinner, hideSpinner, SHOW_SPINNER, HIDE_SPINNER} from '../spinner'

describe('Testing spinner action creators', () => {
  it('should create an action to show spinner', () => {
    const expectedAction = {
      type: SHOW_SPINNER
    }
    expect(showSpinner()).toEqual(expectedAction)
  })

  it('should create an action to hide spinner', () => {
    const expectedAction = {
      type: HIDE_SPINNER
    }
    expect(hideSpinner()).toEqual(expectedAction)
  })
})
