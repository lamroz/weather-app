import {ThunkAction, ThunkDispatch} from 'redux-thunk'

import {RootState} from 'store/reducers'
import {WeatherActionTypes} from 'pages/weather/store/types'
import {SpinnerActionTypes} from './spinner'

export type KnownActions = WeatherActionTypes | SpinnerActionTypes

export type ThunkResult<Result> = ThunkAction<Result, RootState, undefined, KnownActions>
export type ThunkDispatchResult = ThunkDispatch<RootState, undefined, KnownActions>
