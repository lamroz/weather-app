import {SpinnerActionTypes, SHOW_SPINNER, HIDE_SPINNER} from '../actions/spinner'

export interface SpinnerState {
  showSpinner: number
}

const initialState: SpinnerState = {
  showSpinner: 0
}

const spinnerReducer = (state = initialState, action: SpinnerActionTypes): SpinnerState => {
  switch (action.type) {
    case SHOW_SPINNER: {
      return {
        showSpinner: state.showSpinner + 1
      }
    }
    case HIDE_SPINNER: {
      return {
        showSpinner: state.showSpinner - 1
      }
    }
    default:
      return state
  }
}

export default spinnerReducer
