import {combineReducers} from 'redux'

import spinnerReducer from './spinner'

import weatherReducer from 'pages/weather/store/reducer'

const rootReducer = combineReducers({
  spinner: spinnerReducer,
  weather: weatherReducer
})

export type RootState = ReturnType<typeof rootReducer>

export default rootReducer
