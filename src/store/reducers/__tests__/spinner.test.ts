import {HIDE_SPINNER, SHOW_SPINNER} from 'store/actions/spinner'
import spinnerReducer from '../spinner'

describe('Testing spinner reducer', () => {
  it('should return an empty state', () => {
    const expectedState = {
      showSpinner: 0
    }
    expect(spinnerReducer({showSpinner: 0}, {type: null})).toEqual(expectedState)
  })

  it('should show spinner', () => {
    const expectedState = {
      showSpinner: 1
    }
    expect(spinnerReducer({showSpinner: 0}, {type: SHOW_SPINNER})).toEqual(expectedState)
  })

  it('should hid spinner', () => {
    const expectedState = {
      showSpinner: 0
    }
    expect(spinnerReducer({showSpinner: 1}, {type: HIDE_SPINNER})).toEqual(expectedState)
  })
})
