import {createStore, applyMiddleware} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import {useSelector as useReduxSelector, TypedUseSelectorHook} from 'react-redux'

import rootReducer, {RootState} from './reducers'

const composedEnhancer = composeWithDevTools(applyMiddleware(thunk))

const store = createStore(rootReducer, composedEnhancer)

export const useTypedSelector: TypedUseSelectorHook<RootState> = useReduxSelector

export default store
